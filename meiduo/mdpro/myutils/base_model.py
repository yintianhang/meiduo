from django.db import models

# Create your models here.

class BaseModel(models.Model):
    """
    设置抽象类
    """
    create_date=models.DateTimeField(auto_now_add=True,verbose_name='创建时间')
    update_date=models.DateTimeField(auto_now=True,verbose_name='修改时间')
    is_delete=models.BooleanField(default=True,verbose_name='删除标志')
    class Meta:
        abstract=True  #设置为基础类，可以做继承使用