from rest_framework import serializers
from.models import *

class UserSer(serializers.ModelSerializer):
    """
    用户表序列化
    """
    class Meta:
        model=User
        fields='__all__'