from django.db import models
# 引用内置的user模型
from django.contrib.auth.models import AbstractUser
# 导入基础类
from myutils.base_model import BaseModel
# Create your models here.


class User(BaseModel, AbstractUser):
    """
    继承了django的用户表，在这里重写用户表，在里面加字段
    """
    phone = models.CharField(max_length=11, verbose_name='手机号', blank=True, null=True)
    icon = models.ImageField(upload_to='icon', verbose_name='头像', blank=True, null=True)

    class Meta:
        db_table = 'user'

    def __str__(self):
        return self.username


