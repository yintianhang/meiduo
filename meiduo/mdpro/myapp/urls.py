from django.urls import path
from .import views

urlpatterns=[
    path('register/',views.Register.as_view()),  #注册
    path('check_username/',views.CheckUsername.as_view()), #检查用户名
    path('active/', views.ActiveView.as_view()),  # 检查用户名
]